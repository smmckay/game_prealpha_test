﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Distance : object {

    //XY distance between two points
    public static float FindDistance(Rigidbody2D obj1, Rigidbody2D obj2)
    {
        Vector2 dist = new Vector2(obj1.position.x, obj1.position.y) - new Vector2(obj2.position.x, obj2.position.y);
        return Mathf.Sqrt((dist.x * dist.x) + (dist.y * dist.y));
    }

    //XY distance between two points
    public static float FindDistance(Rigidbody2D obj, Vector2 point)
    {
        Vector2 dist = point - new Vector2(obj.position.x, obj.position.y);
        return Mathf.Sqrt((dist.x * dist.x) + (dist.y * dist.y));
    }

    //XY distance between two points
    public static float FindDistance(Vector2 p1, Vector2 p2)
    {
        Vector2 dist = p2 - new Vector2(p1.x, p1.y);
        return Mathf.Sqrt((dist.x * dist.x) + (dist.y * dist.y));
    }
}
