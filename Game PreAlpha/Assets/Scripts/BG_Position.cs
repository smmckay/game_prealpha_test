﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BG_Position : MonoBehaviour {
    public int BG_yOffset = -4;
    public int BG_xOffset = 0;
    private GameObject player;

	// Use this for initialization
	void Start () {
        player = GameObject.Find("Player");
    }
	
	// Update is called once per frame
	void Update () {
        //Update BG Position so it follows the player
        transform.position = new Vector3(player.transform.position.x + BG_xOffset, player.transform.position.y + BG_yOffset);
    }
}
