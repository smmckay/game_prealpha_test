﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {
    GameManager gameManager;
    // Use this for initialization
    void Awake () {
        gameManager = Object.FindObjectOfType<GameManager>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartGame()
    {
        
        gameManager.StartGame(0);
    }

    public void ExitGame()
    {
        gameManager.ExitGame();
    }

    public void ExitApplication()
    {
        gameManager.ExitApplication();
    }


}
