﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DDOL : MonoBehaviour {

    public void Awake()
    {
        //Game Manager persists through all scene changes
        DontDestroyOnLoad(gameObject);
        //Debug.Log("DDOL " + gameObject.name);
    }
}
