﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    public List<string> levelNames;
    public Scene currentLevel;
    public Scene menu;

    public Player p1;
    public LevelManager lvlMgr;
    public int level = 0;

    public float startDelay = 2f;
    public float endDelay = 2f;
    public int score;

    

    public GameObject playerPrefab;
    public GameObject studentEnemyPrefab;
    public GameObject kunaiStudentEnemyPrefab;



    // Use this for initialization
    void Start() {
        levelNames = new List<string>
        {
            "Level1",
            "Level2"
        };
        SceneManager.LoadScene("Menu");
        menu = SceneManager.GetSceneByName("Menu");
        SceneManager.SetActiveScene(menu);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    #region Game State Functions
    public void StartGame(int lvl = 0)
    {
       
        SceneManager.LoadScene(levelNames[level]);
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(levelNames[level]));

        //SetPlayerStart();
        //LoadLevel(currentLevel);  
    }

    public bool NextLevel()
    {
        level += 1;
        score += lvlMgr.lvlScore;
        if (level > levelNames.Count - 1) {
            BeatGame();
            return true;
        } else
        {
            SceneManager.LoadScene(levelNames[level]);
            SceneManager.MoveGameObjectToScene(p1.gameObject, SceneManager.GetSceneByName(levelNames[level]));
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(levelNames[level]));
            return true;
        }
    }

    public void BeatGame()
    {
        lvlMgr.PauseGame();
        lvlMgr.messageText.text = "Congratulations, You are the best Ninja! You got " + score + " points";
        lvlMgr.restartBtn.gameObject.SetActive(false);
    }

    public void ExitGame()
    {
        SceneManager.LoadScene("Menu");
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("Menu"));

    }

    public void ExitApplication()
    {
        Application.Quit();
    }
    #endregion

}
