﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {
    GameManager gameManager;

    private float startWait;
    private float endWait;
    public Text messageText;
    public Text levelScoreText;
    public Button restartBtn;
    public Button exitBtn;

    public List<Vector2> checkpoints = new List<Vector2>();

    public List<Enemy> enemies = new List<Enemy>();
    public float stateTimeElapsed = 0f;
    public float spikeDamage = 3f;
    public float spikeBounciness = 4f;
    public Texture rope;
    public int lvlScore = 0;

    private string gamePausedText = "Game Paused";

    //Testing
    public bool testing = false;

    private enum LevelState
    {
        Loading,
        Starting,
        Playing,
        Paused,
        Ending
    }

    private LevelState lvlState;

    //For Level Testing
    private Player p1;
    private Vector2 LevelBounds = new Vector2(400, 400);
 

    // Use this for initialization
    void Start() {
        //Delays
        
        gameManager = Object.FindObjectOfType<GameManager>();
        messageText = GameObject.Find("MessageText").GetComponent<Text>();
        levelScoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        restartBtn = GameObject.Find("RestartButton").GetComponent<Button>();
        exitBtn = GameObject.Find("ExitButton").GetComponent<Button>();

        restartBtn.gameObject.SetActive(false);
        exitBtn.gameObject.SetActive(false);

        foreach (GameObject e in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            enemies.Add(e.GetComponent<Enemy>());
        }
        if (gameManager)
        {
            gameManager.currentLevel = SceneManager.GetActiveScene();
            startWait = gameManager.startDelay;
            endWait = gameManager.endDelay;
            gameManager.p1 = GameObject.Find("Player").GetComponent<Player>();
            p1 = gameManager.p1;
            gameManager.lvlMgr = this;
            
        } else //Testing specific level, no game manager
        {
            startWait = 2f;
            endWait = 2f;
            p1 = GameObject.Find("Player").GetComponent<Player>();
        }
        lvlState = LevelState.Starting;

    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (lvlState == LevelState.Playing)
            {
                PauseGame();
            }
            else if (lvlState == LevelState.Paused)
            {
                UnPauseGame();
            }
            else if (lvlState == LevelState.Loading && testing)
            {
                UnPauseGame();
            }
            
        }
    }

    private void FixedUpdate()
    {
        switch (lvlState)
        {
            case LevelState.Starting:
                if (GameStarting()) { lvlState = LevelState.Playing;}
                break;
            case LevelState.Playing:
                if (GamePlaying()) { lvlState = LevelState.Ending; }
                break;
            case LevelState.Paused:
                break;
            case LevelState.Ending:
                if (GameEnding()) { ReloadGame(); }
                break;
        }
    }
    #region Main Game Loop



    private bool GameStarting()
    {
        return CheckIfCountdownElapsed(startWait);
    }

    private bool GamePlaying()
    {
        messageText.text = string.Empty;
        levelScoreText.text = "Score " + lvlScore;
        UnPauseGame();
        //While the player is alive we keep playing
        if (gameManager)
        {
            if (!gameManager.p1.IsDead && !OutOfBounds(gameManager.p1))
            {
                return false;
            }
            else
            {
                return true;
            }
         }
         else
             {
             if (!p1.IsDead && !OutOfBounds(p1))
             {
                return false;
             }
             else
             {
                return true;
             }
         }
    }

    public bool GameEnding()
    {
        return CheckIfCountdownElapsed(endWait);
    }

    public void ReloadGame()
    {
        if (gameManager)
        {
            if (checkpoints.Count > 0)
            {

            }
            SceneManager.LoadScene(gameManager.currentLevel.name);
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public void ExitGame()
    {
        if (gameManager)
        {
            SceneManager.LoadScene("Menu");
        }
    }

    private bool OutOfBounds(Player p)
    {
        float x = p.transform.position.x;
        float y = p.transform.position.y;
        return (Mathf.Abs(x) > LevelBounds.x || Mathf.Abs(y) > LevelBounds.y);
    }
    #endregion

    public bool UnPauseGame()
    {
        p1.controller.canMove = true;   //Enable player controls
        restartBtn.gameObject.SetActive(false);
        exitBtn.gameObject.SetActive(false);
        lvlState = LevelState.Playing;
        foreach (Enemy e in enemies)
        {
            e.controller.aiActive = true;
        }
        return true;
    }

    public bool PauseGame()
    {
        messageText.text = gamePausedText;
        restartBtn.gameObject.SetActive(true);
        restartBtn.enabled = true;
        exitBtn.gameObject.SetActive(true);
        exitBtn.enabled = true;
        lvlState = LevelState.Paused;
        p1.controller.canMove = false;
        foreach(Enemy e in enemies)
        {
            e.controller.aiActive = false;
        }
        return true;
    }

    public bool NextLevel()
    {
        if (gameManager)
        {
            return gameManager.NextLevel();
        }
        return false;
    }

    public bool Checkpoint(Vector2 pos, GameObject chk)
    {
        checkpoints.Add(pos);
        chk.SetActive(false);
        return true;
    }

    #region Timer Functions
    public void ResetTimer()
    {
        stateTimeElapsed = 0;
    }

    public bool CheckIfCountdownElapsed(float duration)
    {
        stateTimeElapsed += Time.deltaTime;
        return (stateTimeElapsed >= duration);
    }
    #endregion
}
