﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Character : MonoBehaviour {
    //Character stats, default
    public float startingHealth = 20;
    public float moveSpeed = 5;
    public float climbSpeed = 5;
    public float jumpForce = 20;
    public float health;

    //Time spent in various states
    public float timeDamaged = 0.5f;
    public float timeGrappling = 0.5f;

    //Character initial state
    public Vector2 startPos;

    //Main Camera
    public Camera mainCamera;

    //Character properties
    [HideInInspector] public Vector2 cSize;
    [HideInInspector] public bool facingRight = true;

    //Player Components
    [HideInInspector] public Animator anim;
    [HideInInspector] public Rigidbody2D r2d;
    [HideInInspector] public BoxCollider2D bc2d;

    //Collision Box Properties
    [HideInInspector] public Vector2 collisionBoxSize;
    [HideInInspector] public float groundedSkin = 0.05f;
    [HideInInspector] public LayerMask mask;

    //Healthbar
    public Slider healthbar;
    public Image healthbarFillImage;
    public Color fullHealthColour = Color.green;
    public Color zeroHealthColour = Color.red;

    //Overrideable Functions
    public abstract void TakeDamage(float dmg, float knockBackForce);
    public abstract void OnCollisionEnter2D(Collision2D other);
    public abstract void OnCollisionExit2D(Collision2D other);
    public abstract void OnTriggerEnter2D(Collider2D other);
    public abstract void OnTriggerExit2D(Collider2D other);

    #region Player Stat Properties
    public virtual bool IsDead
    {
        get
        {
            return health <= 0;
        }
    }
    #endregion

    #region MonoBehaviour Functions

    protected virtual void Awake()
    {
        //Get Components
        anim = GetComponent<Animator>();
        bc2d = GetComponent<BoxCollider2D>();
        r2d = GetComponent<Rigidbody2D>();
        healthbar = GetComponentInChildren<Slider>();
        healthbarFillImage = GetComponentsInChildren<Image>()[1];
        mainCamera = Camera.main;
        health = startingHealth;
        healthbar.minValue = 0;
        healthbar.maxValue = this.health;
        
        //transform.position = startPos; //Start position is wherever we place the character

        cSize = Vector2.Scale(bc2d.size, new Vector2(transform.localScale.x, transform.localScale.y)); //Get player's size
        collisionBoxSize = new Vector2(cSize.x / 2, groundedSkin); //Box to test if player is grounded
    }

    protected virtual void Update()
    {
        Animate();
    }
#endregion

    #region Animation Functions
    //Animation parameters that can change each frame
    protected virtual void Animate()
    {
        anim.SetFloat("Speed", Mathf.Abs(r2d.velocity.x));
        anim.SetFloat("Health", health);

        if (r2d.velocity.x > 0.02 && !facingRight || r2d.velocity.x < -0.02 && facingRight)
        {
            Flip();
        }
        SetHealthUI();
        
    }

    //Character flipping
    protected void Flip()
    {
        facingRight = !facingRight;
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
    }

    private void SetHealthUI()
    {
        healthbar.value = this.health;
        healthbarFillImage.color = Color.Lerp(zeroHealthColour, fullHealthColour, health / startingHealth);
    }

    #endregion

}
