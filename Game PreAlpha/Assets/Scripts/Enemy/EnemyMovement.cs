﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Functions to control movement of an enemy character
public class EnemyMovement : ScriptableObject
{
    public float margin = 0.1f; //Margin +- from start or end where we consider that player has reached destination 

    public void Move(Rigidbody2D enemy, Vector2 target, float speed)
    {
       if (MovingRight(enemy, target)) 
        {
           enemy.velocity = new Vector2(speed, enemy.velocity.y);
        } else
        {
            enemy.velocity = new Vector2(speed * -1, enemy.velocity.y);
        }
    }

     //Pull enemy towards player
    public void GrapplePull(Enemy enemy, Vector2 dir, Vector2 grapplePos, float power)
    {
        float dist = FindDistance(enemy.r2d, grapplePos);
        //Debug.Log("Pulling with force" + power + dir + grapplePos);
        enemy.r2d.AddForce(dir * power * -1 * (dist / 100), ForceMode2D.Impulse);
    }


    public bool MovingRight(Rigidbody2D enemy, Vector2 point)
    {
        Vector2 dist = point - new Vector2(enemy.position.x, enemy.position.y);
        float direction = Mathf.Sign(dist.x);
        return (direction == 1);
    }

    //On Hit, force also controls direction
    public void KnockBack(Character enemy, float force)
    {

        Vector2 knockBackForce = force * -1 * new Vector2(enemy.transform.right.x, enemy.transform.up.y) + new Vector2(0, 0.5f);
        enemy.r2d.AddForce(knockBackForce, ForceMode2D.Impulse);
        //Debug.Log("KNOCKED UP");
    }



    //XY distance between two points
    public float FindDistance(Rigidbody2D enemy, Vector2 point)
    {
        Vector2 dist = point - new Vector2(enemy.position.x, enemy.position.y);
        return Mathf.Sqrt((dist.x * dist.x) + (dist.y * dist.y));
    }

    public bool PointReached(Rigidbody2D enemy, Vector2 point, float margin)
    {
        if (Mathf.Abs(enemy.position.x - point.x) < margin)
        {
            return true;
        } else
        {
            return false;
        }
    }
}
