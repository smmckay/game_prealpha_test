
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Monobehaviour: Inherits from character
//Holds enemy data (health, movementspeed) and controls animation of the character in Update

public class Enemy : Character {

    public Vector2[] patrolPoints;
    public bool Stationary = false;

    public GameObject kunai;
    public NPCStateController controller;

    public override bool IsDead
    {
        get
        {
            return health <= 0;
        }
    }

   /** public float MeleeDamage
    {
        get { return meleeAtkDmg; }
        set { meleeAtkDmg = value; }
    } **/

    protected override void Awake()
    {
        base.Awake();
        controller = gameObject.GetComponent<NPCStateController>();
        health = 6;


        if (patrolPoints.Length == 0)   //Default patrol points if none are set up
        {
            throw new System.Exception("No patrol points set for enemy");
        }
        for (int i = 0; i < patrolPoints.Length; i++)   //Set patrol points relative to initial position
        {
            patrolPoints[i].x += transform.position.x;
            patrolPoints[i].y += transform.position.y;
        }

    }

    public void SetMovementSpeed(float mSpeed)
    {
        moveSpeed = mSpeed;
    }

    protected override void Update()
    {
        base.Update();
        Animate();
    }

    //Flip character or set Animator controls to transition between states
    protected override void Animate()
    {
        base.Animate();

    }
    public override void TakeDamage(float dmg, float knockbackForce)
    {
        if (IsDead)
        {
            return;
        }
        //Debug.Log("Enemy Hit");
        health -= dmg;
        if (!IsDead)
        {
            controller.knockBackForce = knockbackForce;
            controller.knocked = Mathf.Abs(knockbackForce) > 0;
            anim.SetTrigger("Hit");
            //TODO: Enemy Knocked Back State
        }
        else
        {
            controller.lvlMgr.lvlScore += controller.enemyStats.score;
            anim.SetTrigger("Die");
        }
    }

    public override void OnCollisionEnter2D(Collision2D other)
    {
        int collisionLayer = other.gameObject.layer;
        if (collisionLayer == LayerMask.NameToLayer("Spikes"))
        {
            TakeDamage(controller.lvlMgr.spikeDamage, 0f);
        }
    }

    public override void OnCollisionExit2D(Collision2D other)
    {
       
    }

    public override void OnTriggerEnter2D(Collider2D other)
    {
        
    }

    public override void OnTriggerExit2D(Collider2D other)
    {
        
    }
}
