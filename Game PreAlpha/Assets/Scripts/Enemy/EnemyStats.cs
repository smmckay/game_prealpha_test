using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/EnemyStats")]
public class EnemyStats : ScriptableObject {
	//Movement
    public float MoveSpeed = 3f;

    //Line of Sight Decision
    public float lookRange = 3;
    public float lookCircleCastRadius = 1f;

    //Chase Range decision
    public float chaseRadius = 10f; 

    //Character stats
    //Ranged Attack Action
    public float rangedAttackSpeed = 10f;
    public float rangedAttackRange = 20f;
    public float rangedAttackCoolDown = 1f;
    public float rangedAttackPower = 2f;
    public float rangedAttackDmg = 1f;

    
    //Attack Action
    public float attackRange = 1f;
    public float attackDelay = 0.5f;
    public float attackContactTime = 0.5f;
    public float attackCoolDown = 3;
    public float attackPower = 3; //Knockback
    public int meleeDmg = 2;

    //Knockback Action
    public float knockbackTime = 0.4f;
    public int score = 10;


}
