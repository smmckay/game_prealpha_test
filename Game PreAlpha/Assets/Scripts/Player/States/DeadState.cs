﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadState : PlayerState
{

    public override void CheckDecisions(PlayerController controller)
    {
        base.CheckDecisions(controller);
    }

    public override void OnEnterState(PlayerController controller)
    {
        controller.player.anim.SetTrigger("Die");
    }

    public override void OnLeaveState(PlayerController controller)
    {
        base.OnLeaveState(controller);
    }

    public override void UpdateState(PlayerController controller)
    {
        
    }
}
