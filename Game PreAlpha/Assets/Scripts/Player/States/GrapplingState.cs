using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplingState : PlayerState {

    public override void CheckDecisions(PlayerController controller)
    {
        base.CheckDecisions(controller);
    }
    public override void OnEnterState(PlayerController controller)
    {
        //Prevent other grapples from going through
        controller.canGrapple = false;
        //If we do not get input here the mouseposition is out of sync
        controller.movement.Grapple((Player)controller.player, Input.mousePosition, controller.player.mainCamera);
    }

    public override void OnLeaveState(PlayerController controller)
    {
        base.OnLeaveState(controller);
    }

    public override void UpdateState(PlayerController controller)
    {

        
        //Check if player leaves grappling state
        if (controller.CheckIfCountdownElapsed(controller.player.timeGrappling))
        {
            controller.PopState();
        }
    }
}
