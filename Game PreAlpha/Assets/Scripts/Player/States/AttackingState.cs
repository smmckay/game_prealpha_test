﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackingState : PlayerState
{

    public override void CheckDecisions(PlayerController controller)
    {
        base.CheckDecisions(controller);
    }

    public override void OnEnterState(PlayerController controller)
    {
        controller.player.anim.SetTrigger("Attack");
        controller.canAttack = false; //Prevent any other attacks from going through
        controller.player.r2d.velocity = new Vector2(0,0);
    }

    public override void OnLeaveState(PlayerController controller)
    {
        base.OnLeaveState(controller);
    }

    public override void UpdateState(PlayerController controller)
    {
        switch (atkSubState)
        {

            case AttackSubState.Idle:
                atkSubState = AttackSubState.Swinging;
                break;
            case AttackSubState.Swinging:
                if (controller.CheckIfCountdownElapsed(controller.player.attackDelay))
                {
                    controller.ResetTimer();
                    atkSubState = AttackSubState.Contacting;
                }
                break;

            case AttackSubState.Contacting:
                if (!controller.CheckIfCountdownElapsed(controller.player.attackContactTime))   //Check for contact within time
                {
                    if (attackHit)
                    {
                        //Do nothing, let contact time run out
                    }
                    else if (controller.movement.Attack(controller.player, controller.player.meleeAtkRange, controller.player.meleeAtkDmg))
                    {
                        attackHit = true;
                    }
                }

                else
                {
                    attackHit = false;
                    atkSubState = AttackSubState.Idle;
                    controller.PopState();
                }
                break;
        }
         switch(controller.playerInput) {
                case PlayerController.PlayerInput.Jump:
                    controller.PopState();
                    controller.PushState(PlayerController.State.Jumping);
                    break;
                case PlayerController.PlayerInput.Grapple:
                    controller.PopState();
                    controller.PushState(PlayerController.State.Grappling);
                    break;
                default:
                    controller.movement.Move(controller.player, xInput);
                    break;
        }
    }
}
