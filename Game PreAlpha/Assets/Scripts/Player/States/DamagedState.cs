﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Damaged suspends player input for an amount of time, instead of checking for input we just have a timer
public class DamagedState : PlayerState
{



    public override void CheckDecisions(PlayerController controller)
    {
        //No ability to move while damaged
        base.CheckDecisions(controller);
    }

    public override void OnEnterState(PlayerController controller)
    {
        controller.player.anim.SetTrigger("Hit");
        if (controller.knocked)
            controller.movement.KnockBack(controller.player, controller.knockBackForce);
    }

    public override void OnLeaveState(PlayerController controller)
    {
        base.OnLeaveState(controller);
    }

    public override void UpdateState(PlayerController controller)
    {
        //Check if player can leave damaged state
        if (controller.CheckIfCountdownElapsed(controller.player.timeDamaged))
        {
            controller.PopState();
        }
    }
}
