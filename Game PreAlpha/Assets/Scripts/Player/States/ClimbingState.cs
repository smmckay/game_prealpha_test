﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimbingState : PlayerState {
    public override void CheckDecisions(PlayerController controller)
    {
        base.CheckDecisions(controller);
    }

    public override void OnEnterState(PlayerController controller)
    {
        //Set gravity to 0 so player can climb 
        controller.player.r2d.gravityScale = 0;
        controller.player.anim.SetBool("Climbing", true);
        controller.player.anim.SetBool("Grounded", false);
    }

    public override void OnLeaveState(PlayerController controller)
    {
        //Reset Gravity on leaving climbing state
        controller.player.r2d.gravityScale = 1f;
        controller.player.anim.SetBool("Climbing", false);
        base.OnLeaveState(controller);
    }

    public override void UpdateState(PlayerController controller)
    {
        switch(controller.playerInput)
        {
            case PlayerController.PlayerInput.Attack:
                break;
            case PlayerController.PlayerInput.Grapple:
                controller.TransitionToState(PlayerController.State.Grappling);
                break;
            case PlayerController.PlayerInput.Jump:
                controller.TransitionToState(PlayerController.State.Jumping);
                break;
            default:
                controller.movement.Climb(controller.player, yInput);
                controller.movement.Move(controller.player, xInput);
                break;
        }        
    }
}
