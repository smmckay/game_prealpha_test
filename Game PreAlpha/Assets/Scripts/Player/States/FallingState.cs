﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingState : IdleState
{

    public override void CheckDecisions(PlayerController controller)
    {
        base.CheckDecisions(controller);
    }

    public override void OnEnterState(PlayerController controller)
    {
        base.OnEnterState(controller);
    }

    public override void OnLeaveState(PlayerController controller)
    {
        base.OnLeaveState(controller);
    }

    public override void UpdateState(PlayerController controller)
    {
        if (groundedSwitch) {
            controller.PopState();
        } else {


            switch(controller.playerInput) {
                case PlayerController.PlayerInput.Grapple:
                    controller.PushState(PlayerController.State.Grappling);
                    break;
                default:
                    controller.movement.Move(controller.player, xInput);
                    break;
            }
        }
    }
}
