using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Player not moving, can move, jump, attack and grapple from this state
public class IdleState : PlayerState {
    public override void CheckDecisions(PlayerController controller)
    {
        base.CheckDecisions(controller);

    }

    public override void UpdateState(PlayerController controller)
    {
        #region Transitions
        if (!groundedSwitch)    //If we've left the ground
        {
            controller.PushState(PlayerController.State.Falling);
        } else {


            switch(controller.playerInput) {
                case PlayerController.PlayerInput.Jump:
                    controller.PushState(PlayerController.State.Jumping);
                    break;
                case PlayerController.PlayerInput.Attack:
                    controller.PushState(PlayerController.State.Attacking);
                    break;
                case PlayerController.PlayerInput.Grapple:
                    controller.PushState(PlayerController.State.Grappling);
                    break;
                default:
                    controller.movement.Move(controller.player, xInput);
                    break;
            }
        }
        #endregion
    }

    public override void OnEnterState(PlayerController controller)
    {
        
    }

    public override void OnLeaveState(PlayerController controller)
    {
        base.OnLeaveState(controller);
    }


}
