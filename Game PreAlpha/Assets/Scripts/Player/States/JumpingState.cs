﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingState : PlayerState
{
    public override void CheckDecisions(PlayerController controller)
    {
        base.CheckDecisions(controller);
    }

    public override void OnEnterState(PlayerController controller)
    {
        controller.movement.Jump(controller.player);
    }

    public override void OnLeaveState(PlayerController controller)
    {
        base.OnLeaveState(controller);
    }

    public override void UpdateState(PlayerController controller)
    {
        
        controller.TransitionToState(PlayerController.State.Falling);    
    }

}
