﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateParams : ScriptableObject {

    public virtual bool SetParams(PlayerController controller, PlayerController.State s, params float[] list)
    {
        switch (s)
        {
            case PlayerController.State.Attacking:
                return true;
            case PlayerController.State.Climbing:
                return true;
            case PlayerController.State.Damaged:
                controller.knockBackForce = list[0];
                controller.knocked = Mathf.Abs(controller.knockBackForce) > 0;
                return true;
            case PlayerController.State.Dead:
                return true;
            case PlayerController.State.Falling:
                return true;
            case PlayerController.State.Grappling:
                return true;
            case PlayerController.State.Idle:
                return true;
            case PlayerController.State.Jumping:
            default:
                return false;

        }
    }


    public virtual bool ResetParams(PlayerController controller)
    {
        controller.canAttack = true;
        controller.knockBackForce = 0f;
        controller.knocked = false;
        return true;
    }
}
