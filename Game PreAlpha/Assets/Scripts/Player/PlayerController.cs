using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Player Controller is different from Enemy, States control the expected behaviour of the player's character on different inputs, and what inputs are available for them
//Transitions correspond with those found in the Animator, when we expect different behaviour
public class PlayerController : MonoBehaviour {

    //Knockback Behaviour
    [HideInInspector] public bool knocked; //If player is knocked back
    [HideInInspector] public float knockBackForce; //Used for determining direction of knockback

    //Player
    public Player player;

    //States
    public State initialState = State.Idle;
    public StateParams initialParams;

    //Level Manager
    public LevelManager lvlMgr;

    [HideInInspector] public Stack<PlayerState> stateStack;
    
    [HideInInspector] public enum State
    {
        Idle, //Includes moving
        Climbing,
        Jumping,
        Falling,
        Attacking,
        Grappling,
        Damaged,
        Dead
    }

    [HideInInspector] public Dictionary<State, PlayerState> stateLookup;

    //Store next state
    private PlayerState nextState;

    [HideInInspector] public StateParams stateParams;
    

    //Movement script to use
    public PlayerMovement movement;

    //Time in state
    private float stateTimeElapsed;

    //Input
    [HideInInspector] public enum PlayerInput
    {
        Nothing,
        Jump,
        Attack,
        Grapple,
    }
    [HideInInspector] public PlayerInput playerInput;

    [HideInInspector] public bool canJump = true;
    [HideInInspector] public bool canAttack = true;
    [HideInInspector] public bool canGrapple = true;
    [HideInInspector] public bool canMove = false;
    public bool testing = false;

    private Vector2 lastPos;
    private Vector2 currentPos;


    #region MonoBehaviour Functions
    // Use this for initialization
    void Awake() {

        stateLookup = new Dictionary<State, PlayerState>
        {
            { State.Idle,  ScriptableObject.CreateInstance<IdleState>() },
            { State.Climbing, ScriptableObject.CreateInstance<ClimbingState>() },
            { State.Jumping, ScriptableObject.CreateInstance <JumpingState>() },
            { State.Falling, ScriptableObject.CreateInstance <FallingState>() },
            { State.Attacking, ScriptableObject.CreateInstance <AttackingState>() },
            { State.Grappling, ScriptableObject.CreateInstance <GrapplingState>() },
            { State.Damaged, ScriptableObject.CreateInstance <DamagedState>() },
            { State.Dead, ScriptableObject.CreateInstance <DeadState>() }
        };
        stateStack = new Stack<PlayerState>();
        player = GetComponent<Player>(); //Get reference to the character so we can update stats and animations
        lvlMgr = GameObject.FindObjectOfType<LevelManager>();
        movement = ScriptableObject.CreateInstance <PlayerMovement>();    //Movement script used to control character
        stateParams = ScriptableObject.CreateInstance<StateParams>(); //To pass params into a state
        PushState(initialState); //Initial state
    }

    //Update Each Time Step
    private void FixedUpdate()
    {
        if (stateStack != null & stateStack.Count > 0)
        {
            stateStack.Peek().UpdateState(this);

        }
        CheckCollisions();
    }


    private void CheckCollisions() {
        currentPos = player.transform.position;
        if (!(lastPos == new Vector2(0,0))) {
            RaycastHit2D hit;
            
            float dist = movement.FindDistance(player.transform.position, lastPos);
            Vector2 moved = currentPos - lastPos;
            Debug.DrawLine(currentPos, lastPos);
            hit = Physics2D.CircleCast(currentPos, 0.01f, moved, dist, LayerMask.GetMask("Ground"));
            if(hit && !hit.collider.CompareTag("Wood")) {
                //Debug.Log("Passing Through");
                player.transform.position = lastPos;
                player.r2d.velocity = new Vector2(0f, player.r2d.velocity.y);
            }
        }
        lastPos = currentPos;
    }

    //Update Each Frame
    private void Update()
    {
        if (stateStack != null & stateStack.Count > 0)
        {
            stateStack.Peek().CheckDecisions(this);
        }
        
    }
    //TODO: Set up debugging script
    private void OnDrawGizmos()
    {
        if (player != null)
        {
            Vector2 boxCenter = (Vector2)player.transform.position + Vector2.down * (player.cSize.y + player.collisionBoxSize.y) * 0.5f;
            Gizmos.DrawWireCube(new Vector3(boxCenter.x, boxCenter.y), new Vector3(player.collisionBoxSize.x, player.collisionBoxSize.y));
        }

    }

    #endregion

    public bool SetParams(State s, params float[] list)
    {
        return (this.stateParams.SetParams(this, s, list));
    } 

    public bool ResetParams()
    {
        return (this.stateParams.ResetParams(this));
    }

    #region Stack Functions
    //Push state to top of stack
    public bool PushState(State next, params float[] list)
    {
        nextState = stateLookup[next];
        if (stateStack.Count == 0)
        {
            //Debug.Log(next.ToString());
            SetParams(next, list);
            stateStack.Push(nextState);
            stateStack.Peek().OnEnterState(this);
            return true;
        }
        else if (nextState != stateStack.Peek())
        {
            //Debug.Log(next.ToString());
            SetParams(next, list);
            stateStack.Push(nextState);
            stateStack.Peek().OnEnterState(this);
            return true;
        }
        else
        {
            return false;
        }
    }

    //Pop state from the top of the stack
    public PlayerState PopState()
    {
        
        ResetTimer();
        stateStack.Peek().OnLeaveState(this);
        PlayerState lastState = stateStack.Pop();
        //Debug.Log("Leaving State");
        ResetParams();
        OnExit();
        return lastState;

    }


    //Pop last state from the stack, push given
    public bool TransitionToState(State next, params float[] list)
    {
        nextState = stateLookup[next];
        if (nextState != stateStack.Peek())
        {
            //Debug.Log(next.ToString());
            stateStack.Peek().OnLeaveState(this);
            stateStack.Pop();
            ResetParams();
            OnExit();
            SetParams(next, list);
            stateStack.Push(nextState);
            stateStack.Peek().OnEnterState(this);
            return true;
        }
        else
        {
            return false;
        }
    }
    #endregion

    #region Timer Functions
    public void ResetTimer()
    {
        stateTimeElapsed = 0;
    }

    public void OnExit()
    {
        canAttack = true;
    }

    public bool CheckIfCountdownElapsed(float duration)
    {
        stateTimeElapsed += Time.deltaTime;
        return (stateTimeElapsed >= duration);
    }
    #endregion
}
