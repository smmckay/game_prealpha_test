
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : ScriptableObject {


    private float vMargin = 0.01f;  //Margin in which movement does not trigger a state change
   


    public bool Attack(Player player, float range, float dmg)
    {
        Collider2D hit;
        Enemy target;

        hit = Physics2D.OverlapBox(player.transform.position, new Vector2(player.meleeAtkRange * 2 + player.cSize.x, player.cSize.y), 0, LayerMask.GetMask("Enemy"));
        if (hit)
        {
            target = hit.gameObject.GetComponent<Enemy>();
            if (!target.IsDead)
            {
                target.TakeDamage(player.meleeAtkDmg, 0);
                target.controller.DelayTime = target.controller.enemyStats.knockbackTime;
                target.controller.enemyMovement.KnockBack(target, (player.transform.position - target.transform.position).normalized.x * player.meleeAtkForce);
                return true;
            }
            return false;
        }
        return false;
    }
    //On Hit, force also controls direction
    public void KnockBack(Character player, float force)
    {
        
        Vector2 knockBackForce = force * -1 * new Vector2(player.transform.right.x, player.transform.up.y) + new Vector2(0, 0.5f);
        player.r2d.AddForce(knockBackForce, ForceMode2D.Impulse);
        //Debug.Log("KNOCKED UP");
    }

    //Jumping
    public void Jump(Character player)
    {
        player.r2d.AddForce(Vector2.up * player.jumpForce, ForceMode2D.Impulse);
    }

    //Moving on ground
    //Input in x axis
    public bool Move(Character player, float input)
    {
        float oldV = player.r2d.velocity.x;
        player.r2d.velocity = new Vector2(input * player.moveSpeed, player.r2d.velocity.y);
        float newV = player.r2d.velocity.x;

        return Mathf.Abs(newV - oldV) > vMargin;    //Return true if player has moved
    }

    //Launch the grappling hook
    public void Grapple(Player player, Vector3 mousePosition, Camera mainCam)
    {
    	//TODO: pass in Camera reference to movement function
    	float aspect = mainCam.aspect;
        //Calculate the direction to fire off the grappling hook
        Vector3 playerPosition = mainCam.WorldToViewportPoint(player.transform.position);
        playerPosition.z = 0;
        Vector3 grappleDirection = (mainCam.ScreenToViewportPoint(mousePosition) - playerPosition);
       	grappleDirection.x *= mainCam.aspect;

        //Calculate the angle the grappling hook should be positioned based on the direction
        Quaternion grapplingAngle = Quaternion.Euler(0, 0, Mathf.Atan2(grappleDirection.y, grappleDirection.x) * Mathf.Rad2Deg - 90);

        //Create the grappling hook
        GrapplingHook grapplingHook = Instantiate(player.grapplingHook, player.transform.position, grapplingAngle).GetComponent<GrapplingHook>();
        grapplingHook.Initialize(player, grappleDirection); // We give the grappling hook a reference to the player to get stats and also position to pull the player to
    }

    //Pull player towards grappling hook, TODO: force scale with distance
    public void GrapplePull(Player player, Vector2 dir, Vector2 grapplePos)
    {
    	float dist = FindDistance(player.transform.position, grapplePos);
        player.r2d.AddForce(dir * player.grappleForce * (dist / 100), ForceMode2D.Impulse);
    }


    public void LogCoordinate(Vector3 coord, String coordName = "")
    {
        //Debug.Log(coordName + " X:" + coord.x + " Y:" + coord.y + " Z:" + coord.z);
   
    }

    //Climbing up a ladder, assume the gravity scale set to 0
    //Input in y axis
    public void Climb(Character player, float input)
    {
        player.r2d.velocity = new Vector2(player.r2d.velocity.x, input * player.climbSpeed);
    }

     //XY distance between two points
    public float FindDistance(Vector2 p1, Vector2 p2)
    {
        Vector2 dist = p2 - new Vector2(p1.x, p1.y);
        return Mathf.Sqrt((dist.x * dist.x) + (dist.y * dist.y));
    }
}
