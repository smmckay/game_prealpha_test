using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerState : ScriptableObject {

    //Different inputs taken in CheckDecisions, transition to other states also happen in CheckDecisions
    //Inputs handled in UpdateState

    public float xInput; //Used for moving
    public float yInput; //Used for climbing
    public Vector2 cursorPosition; //Used for aiming the grapple and ranged attack 
    public bool groundedSwitch;
    protected bool attackHit = false;

    public enum AttackSubState
    {
        Idle,
        Swinging,
        Contacting,
    }

    public AttackSubState atkSubState;


    //Called each physics update
    public abstract void UpdateState(PlayerController controller);

    //Called each frame 
    public virtual void CheckDecisions(PlayerController controller)
    {
        //Check if box at player's feet intersects with any colliders in the ground or tree layers to determine if they are on the ground
        Vector2 boxCenter = (Vector2)controller.player.transform.position + Vector2.down * (controller.player.cSize.y + controller.player.collisionBoxSize.y) * 0.5f;
        groundedSwitch = Physics2D.OverlapBox(boxCenter, controller.player.collisionBoxSize, 0f, (1 << 9) | (1 << 10)) != null;
        controller.player.anim.SetBool("Grounded", groundedSwitch);
        controller.canJump = groundedSwitch && Mathf.Abs(controller.player.r2d.velocity.y) <= 0.05;

        //Check if the player can move (game not paused or anything)
        if (controller.canMove || controller.testing)
        {
            //Always check for movement 
            xInput = Input.GetAxis("Horizontal");

            //Always check for movement 
            yInput = Input.GetAxis("Vertical");

            //Transition to jumping if possible
            if (Input.GetButton("Jump") && controller.canJump)
            {
                controller.playerInput = PlayerController.PlayerInput.Jump;
            }
            //Transition to attacking if possible
            if (Input.GetButtonDown("MeleeAttack") && controller.canAttack)
            {
                controller.playerInput = PlayerController.PlayerInput.Attack;
            }
            //Transition to grappling if possible
            if (Input.GetButton("Grapple") && controller.canGrapple)
            {
                controller.playerInput = PlayerController.PlayerInput.Grapple;
            }
        }

    }

    //Called on entering the state
    public virtual void OnEnterState(PlayerController controller)
    {

    }

    //Called before leaving the state
    //Reset input
    public virtual void OnLeaveState(PlayerController controller)
    {
        xInput = 0.0f;
        yInput = 0.0f;
        controller.playerInput = PlayerController.PlayerInput.Nothing;
    }
}
