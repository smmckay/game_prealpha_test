﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character {

    
    //Damage settings for melee
    public float attackDelay = 0.2f;
    public float attackContactTime = 0.5f;
    public float meleeAtkDmg = 2;
    public float meleeAtkRange = 1f;
    public float meleeAtkForce = 8f;

    //Grappling hook settings
    public float grappleMaxPullForce;
    public float grappleSpeed = 100f; //Speed that grapple travels away from player
    public float grappleForce = 5f; //Impulse force exerted on player when pulling towards grapple target
    public float grappleDamage = 1f; //Damage Done by grappling hook
    public float grappleTime = 2f; //Time grapple is active before dissapearing
    public float grapplePullTime = 0.5f; //Time grapple is pulling the player
    public GameObject grapplingHook;

    //Player Controller
    public PlayerController controller;


    //Set initial properties of player
    protected override void Awake()
    {
        base.Awake();
        controller = gameObject.GetComponent<PlayerController>();
    }

    //Character takes damage, affects state and stats
    public override void TakeDamage(float dmg, float knockBackForce)
    {
        health -= dmg;
        if (!IsDead)
        {
            //Push damaged state, pop when time is up
            controller.PushState(PlayerController.State.Damaged, knockBackForce);

        }
        else
        {
            //Transition to dead state
            controller.TransitionToState(PlayerController.State.Dead);
        }
    }

    public override void OnTriggerEnter2D(Collider2D other)
    {
        int collisionLayer = other.gameObject.layer;
        if (collisionLayer == LayerMask.NameToLayer("Ladders"))
        {

            controller.PushState(PlayerController.State.Climbing);
        }
        else if (collisionLayer == LayerMask.NameToLayer("Checkpoint"))
        {
            controller.lvlMgr.Checkpoint(transform.position, other.gameObject);
        }
        else if (collisionLayer == LayerMask.NameToLayer("Finish"))
        {
            controller.lvlMgr.NextLevel(); //Go to the next level if we've reached the end
        }
        
    }

    public override void OnTriggerExit2D(Collider2D other)
    {
        int collisionLayer = other.gameObject.layer;
        if (collisionLayer == LayerMask.NameToLayer("Ladders"))
        {
             if (controller.stateStack != null & controller.stateStack.Count > 0) {
                if (controller.stateStack.Peek() == controller.stateLookup[PlayerController.State.Climbing]) {
                    controller.PopState(); //May change, as state when leaving isn't necessarily the same as when entering. e.g. climb ladder from ground then fall off into air
                }
             } 
        }
    }
    //When Character collides with any other gameobject, a change in state can be triggered
    public override void OnCollisionEnter2D(Collision2D other)
    {
        int collisionLayer = other.gameObject.layer;
        if (collisionLayer == LayerMask.NameToLayer("Spikes"))
        {
            TakeDamage(controller.lvlMgr.spikeDamage, 0f);
        }
    }

    public override void OnCollisionExit2D(Collision2D other)
    {
        int collisionLayer = other.gameObject.layer;
    }
}
