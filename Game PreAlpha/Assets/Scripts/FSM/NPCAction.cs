﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class NPCAction : ScriptableObject {

    public abstract void Do(NPCStateController controller);
}
