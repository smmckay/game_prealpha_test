﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Controls fixedUpdate of enemy character using StateController
//References other scripts for the enemy
//Adapted from tutorial on pluggable AI with scriptable objects available here: https://unity3d.com/learn/tutorials/s/navigation
public class NPCStateController : MonoBehaviour {

    //States
    public NPCState currentState;

    public enum AttackSubstates
    {
        Idle,
        Swinging,
        Contacting,
    }

    public enum RangedAttackSubstates
    {
        Idle, 
        Throwing
    }

    //Instance specific enemy classes
    public Enemy enemy; // Number 1 1
    public EnemyMovement enemyMovement;
    public EnemyStats enemyStats;
    public LevelManager lvlMgr;



    //Properties of enemy class
    [HideInInspector] public Rigidbody2D enemyRigidbody2D;
    [HideInInspector] public Animator enemyAnim;
    [HideInInspector] public Vector2 enemySize;

    //Controlled by game manager
    [HideInInspector] public bool aiActive = false;


    //Action specific 

    //Patrol action
    //Target position for patrolling
    [HideInInspector] public Vector2 target;

    //Chase action
    //Target character for chasing (most likely the player)
    [HideInInspector] public GameObject targetCharacter;

    //Attack action
    [HideInInspector] public bool firstAttack = true;
    [HideInInspector] public AttackSubstates attackSubstate = AttackSubstates.Idle;

    //Ranged attack action
    [HideInInspector] public bool rangedFirstAttack = true;
    [HideInInspector] public RangedAttackSubstates rangedAttackSubstate = RangedAttackSubstates.Idle;

    //Timer
    [HideInInspector] public float totalTimeElapsed;
    [HideInInspector] public float lastTime;




    //Reset on state change
    [HideInInspector] public float stateTimeElapsed;
    [HideInInspector] public int pIndex = 0;
    [HideInInspector] public bool movingForwards;
    [HideInInspector] public bool hitWall;
    [HideInInspector] public bool aboutToFall;
    //Knockback
    [HideInInspector] public bool knocked;
    [HideInInspector] public float knockBackForce;

    [HideInInspector] public Vector2 lastPos;
    [HideInInspector] public Vector2 currentPos;
    private float delayTime; //Used for delaying the enemy's actions in the case of being hit

    public float DelayTime
    {
        get { return delayTime; }
        set {
            ResetTimer();
            delayTime = value;
        }
    }



    private void Awake()
    {
        aiActive = false;
        //Get Components from GameObject instance
        enemy = GetComponent<Enemy>();
        enemy.SetMovementSpeed(enemyStats.MoveSpeed);
        enemyMovement = ScriptableObject.CreateInstance<EnemyMovement>();
        lvlMgr = GameObject.FindObjectOfType<LevelManager>();
        enemyAnim = GetComponent<Animator>();
        enemyRigidbody2D = GetComponent<Rigidbody2D>();
        enemySize = Vector2.Scale(enemy.GetComponent<BoxCollider2D>().size, (new Vector2(enemy.transform.localScale.x, enemy.transform.localScale.y)));

    }

    // Fixed Update is called a fixed amount of times per second
    // We update State in fixed update because of the physics calculations involved in movement
    // Will have to change when dealing with input and animations in controller
    private void FixedUpdate()
    {
        //Set in game manager / level manager
        if (!aiActive)
            return;

        if (!(delayTime == 0)) 
        {
            if (CheckIfCountdownElapsed(delayTime))
            {
                delayTime = 0;
                ResetTimer();
            }
            
            else
            {
                return;
            }
            
        }

        currentState.UpdateState(this); 
        CheckCollisions();
    }

    private void CheckCollisions() {
        currentPos = enemy.transform.position;
        if (!(lastPos == new Vector2(0,0))) {
            RaycastHit2D hit;
            
            float dist = enemyMovement.FindDistance(enemy.r2d, lastPos);
            Vector2 movement = currentPos - lastPos;
            Debug.DrawLine(currentPos, lastPos);
            hit = Physics2D.CircleCast(currentPos, 0.01f, movement, dist, LayerMask.GetMask("Ground"));
            if(hit) {
                //Debug.Log("Passing Through");
                enemy.transform.position = lastPos;
            }
        }
        lastPos = currentPos;
    }

    //TODO: Set up debugging script
    private void OnDrawGizmos()
    {
        if (currentState != null && enemy != null)
        {
            Gizmos.color = currentState.sceneGizmoColor;
            Gizmos.DrawWireCube(enemy.transform.position, new Vector3(enemyStats.attackRange * 2, enemySize.y, 0));
        }
    }

    //Go to the next state, returns true if state did change
    public bool TransitionToState(NPCState nextState) 
    {
        //May change.. could change to a different patrolling state?
        if (nextState != currentState)
        {
            currentState = nextState;
            OnExitState();
            //Debug.Log("Changing to " + nextState.name);
            return true;
        }
        else
        {
            return false;
        }
    } 

    public void SaveTime()
    {
        lastTime = totalTimeElapsed;
    }

    public float GetTime()
    {
        totalTimeElapsed += Time.deltaTime;
        return totalTimeElapsed;
    }

    public bool CheckIfCountdownElapsed(float duration)
    {
        stateTimeElapsed += Time.deltaTime;
        return (stateTimeElapsed >= duration);
    }

    private void OnExitState()
    {
        knocked = false;
        knockBackForce = 0f;
        stateTimeElapsed = 0;
        firstAttack = true;
        attackSubstate = AttackSubstates.Idle;
    }

    public void ResetTimer()
    {
        stateTimeElapsed = 0;
    }
}
