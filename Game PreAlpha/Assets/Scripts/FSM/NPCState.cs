﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/State")]
public class NPCState : ScriptableObject
{
    public NPCAction[] actions;
    public NPCTransition[] transitions;
    public Color sceneGizmoColor = Color.grey;

    public void UpdateState(NPCStateController controller)
    {
        DoActions(controller);
        CheckTransitions(controller);
    }

    public void UpdateAnimation(NPCStateController controller)
    {
       
    }

    private void DoActions(NPCStateController controller)
    {
        for (int i = 0; i < actions.Length; i++)
        {
            actions[i].Do(controller);
        }
    }

    private void CheckTransitions(NPCStateController controller)
    {
        for (int i = 0; i < transitions.Length; i++)
        {
            if (transitions[i].decision.Decide(controller))
            {
                if (controller.TransitionToState(transitions[i].trueState)) {
                    return;
                }
            } else
            {
                if (controller.TransitionToState(transitions[i].falseState))
                {
                    return;
                }
            }

        }
    }
   
}
