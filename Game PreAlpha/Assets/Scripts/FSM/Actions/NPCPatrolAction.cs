﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Patrol")]
public class NPCPatrolAction : NPCAction
{

    private Vector2[] pathPoints;
    


	public override void Do(NPCStateController controller)
    {
        Patrol(controller);
    }

    private void Patrol(NPCStateController controller)
    {
        if (!controller.enemy.Stationary)
        {
            if (controller.pIndex == -1)
            {
                controller.pIndex = 0;
            }
            pathPoints = controller.enemy.patrolPoints;
            //bool hitsomething;
            //RaycastHit2D onGround;
            //float colSize = 0.01f;
            //Vector2 pos = controller.enemy.transform.position;
            //Vector2 fw;
            //if (controller.movingForwards)
            //{
            //    fw = controller.enemy.transform.right;
            //} else
            //{
            //    fw = controller.enemy.transform.right * -1;
            //}
           
            //Vector2 down = controller.enemy.transform.up * -1;
            //LayerMask obstacles = LayerMask.GetMask("Ground", "Trees");
            //ContactFilter2D filter = new ContactFilter2D();
            //filter.layerMask = obstacles;
            //Collider2D[] collisions = new Collider2D[2];
            //controller.enemy.GetComponent<Collider2D>().OverlapCollider(filter, collisions);
            //hitsomething = collisions[1] != null;
            //onGround = Physics2D.CircleCast(pos + (fw * (controller.enemy.cSize.x)), colSize, down, controller.enemy.cSize.y, obstacles);

            //if (!onGround)
            //{
            //    controller.aboutToFall = true;
            //    //Debug.Log("I'm not on the ground and moving forward =" + controller.movingForwards.ToString());
            //}

            //if (hitsomething)
            //{
            //    controller.hitWall = true;
            //    //Debug.Log("Stuck");
            //}


            //Debug.Log(controller.enemy.ToString() + " " + controller.pIndex);
            if ((!controller.enemyMovement.PointReached(controller.enemyRigidbody2D, pathPoints[controller.pIndex], 0.1f)) )//&& !controller.aboutToFall && !controller.hitWall)
            {
                controller.enemyMovement.Move(controller.enemyRigidbody2D, pathPoints[controller.pIndex], controller.enemy.moveSpeed);
            }
            else

            {
                if (controller.movingForwards)
                {
                    controller.pIndex += 1;
                    //Debug.Log(controller.enemy.ToString() + " " + controller.pIndex);
                }
                else
                {
                    controller.pIndex -= 1;
                    //Debug.Log(controller.enemy.ToString() + " " + controller.pIndex);
                }
                if (controller.pIndex >= pathPoints.Length - 1 && controller.movingForwards || controller.pIndex <= 0 && !controller.movingForwards)
                {
                    controller.movingForwards = !controller.movingForwards;
                }
                //controller.hitWall = false;
                //controller.aboutToFall = false;
            }
        }
    }
}
