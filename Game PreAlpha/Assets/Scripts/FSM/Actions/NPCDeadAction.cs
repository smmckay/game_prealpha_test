﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Dead")]
public class NPCDeadAction : NPCAction {
    public override void Do(NPCStateController controller)
    {
       //Do Nothing when we are dead
    }

}
