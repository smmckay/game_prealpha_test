﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/RangedAttack")]
public class NPCRangedAttackAction : NPCAction
{


	public override void Do(NPCStateController controller)
    {
        RangedAttack(controller);
    }

	
    private void RangedAttack(NPCStateController controller)
    {//Ranged attack needs to know how long was spent in other states after the last ranged attack  (lastTime)

    	switch (controller.rangedAttackSubstate)
    	{
    		case NPCStateController.RangedAttackSubstates.Idle:
                Kunai kunai = Instantiate(controller.enemy.kunai, controller.enemy.transform).GetComponent<Kunai>();
                controller.SaveTime();
                controller.rangedAttackSubstate = NPCStateController.RangedAttackSubstates.Throwing;
                kunai.Initialize(controller.enemy, controller.enemy.transform.right * Mathf.Sign(controller.enemy.transform.localScale.x));
	    		break;

    		case NPCStateController.RangedAttackSubstates.Throwing:

    			if (controller.CheckIfCountdownElapsed(controller.enemyStats.rangedAttackCoolDown)) {
                    controller.ResetTimer();
                    controller.rangedAttackSubstate = NPCStateController.RangedAttackSubstates.Idle;
                }
    			else if (controller.GetTime() > controller.lastTime + controller.enemyStats.rangedAttackCoolDown + controller.enemyStats.rangedAttackCoolDown * 0.2) {
    				//Debug.Log("Last time" + controller.lastTime);
    				//Debug.Log("Current time" + controller.GetTime());
    				controller.ResetTimer();
    				controller.rangedAttackSubstate = NPCStateController.RangedAttackSubstates.Idle;
    			}

    			break;
    	}
    }
}
