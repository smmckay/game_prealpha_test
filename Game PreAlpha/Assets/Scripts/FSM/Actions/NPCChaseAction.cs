﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Chase")]
public class NPCChaseAction : NPCAction {
    public override void Do(NPCStateController controller)
    {
        Chase(controller);
    }


    //Follow the player to within margin 1/5 attack range
    //TODO: Pathfinding (jumping and falling)
    private void Chase(NPCStateController controller)
    {
        if (!(controller.enemyMovement.PointReached(controller.enemyRigidbody2D, new Vector2(controller.targetCharacter.transform.position.x, controller.targetCharacter.transform.position.y), controller.enemyStats.attackRange / 5)))
        {
            controller.enemyMovement.Move(controller.enemyRigidbody2D, controller.targetCharacter.transform.position, controller.enemy.moveSpeed);
        }
       
    }

}
