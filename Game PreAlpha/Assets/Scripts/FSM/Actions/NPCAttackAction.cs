﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "PluggableAI/Actions/Attack")]
public class NPCAttackAction : NPCAction
{

    public override void Do(NPCStateController controller)
    {
        Attack(controller);
    }



    private void Attack(NPCStateController controller)
    {
        Collider2D hit = new Collider2D();
        Character target;
        


        switch(controller.attackSubstate)

        {
            case NPCStateController.AttackSubstates.Idle:
                if ((controller.firstAttack || controller.CheckIfCountdownElapsed(controller.enemyStats.attackCoolDown)))
                {
                    controller.ResetTimer();
                    controller.enemy.anim.SetTrigger("Attack");
                    controller.firstAttack = false;
                    controller.attackSubstate = NPCStateController.AttackSubstates.Swinging;

                }
                break;


            case NPCStateController.AttackSubstates.Swinging:
                if (controller.CheckIfCountdownElapsed(controller.enemyStats.attackDelay)) {
                    controller.ResetTimer();
                    controller.attackSubstate = NPCStateController.AttackSubstates.Contacting;
                    
                }
                break;

            case NPCStateController.AttackSubstates.Contacting:
                if (!controller.CheckIfCountdownElapsed(controller.enemyStats.attackContactTime))
                {
                    hit = Physics2D.OverlapBox(controller.enemy.transform.position, new Vector2(controller.enemyStats.attackRange * 2 + controller.enemySize.x, controller.enemySize.y), 0, LayerMask.GetMask("Player"));
                    //If we hit the player within the contact area time the attack is over
                    if (hit && hit.CompareTag("Player"))
                    {
                        target = hit.gameObject.GetComponent<Character>();
                        target.TakeDamage(controller.enemyStats.meleeDmg, (controller.enemy.transform.position - target.transform.position).normalized.x * controller.enemyStats.attackPower);
                        controller.ResetTimer();
                        controller.attackSubstate = NPCStateController.AttackSubstates.Idle;
                    }
                    
                }
                //If we do not hit anything within the contact area time the attack is over
                else
                {
                    controller.ResetTimer();
                    controller.attackSubstate = NPCStateController.AttackSubstates.Idle;
                }
                break;


        }  
    }
}
