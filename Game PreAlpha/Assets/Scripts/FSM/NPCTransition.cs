﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NPCTransition
{
    public NPCDecision decision;
    public NPCState trueState;
    public NPCState falseState;
}
