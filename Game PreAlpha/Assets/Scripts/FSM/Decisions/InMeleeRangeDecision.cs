﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/InMeleeRange")]
public class InMeleeRangeDecision : NPCDecision
{


    public override bool Decide(NPCStateController controller)
    {
        return InRange(controller);
    }
    private bool InRange(NPCStateController controller)
    {
        RaycastHit2D hit;
        Debug.DrawLine(controller.enemy.transform.localPosition, controller.enemy.transform.localPosition + controller.enemy.transform.right * Mathf.Sign(controller.enemy.transform.localScale.x) * controller.enemyStats.attackRange, Color.red);

        //Check if within melee attack range of the enemy
        hit = Physics2D.CircleCast(controller.enemy.transform.position, controller.enemyStats.lookCircleCastRadius, controller.enemy.transform.right * Mathf.Sign(controller.enemy.transform.localScale.x), controller.enemyStats.attackRange / 2, LayerMask.GetMask("Player"));
        if (hit && hit.collider.CompareTag("Player"))
        {
            //Set the character (player) to be targetted
            controller.targetCharacter = hit.collider.gameObject;
            return true;
        }
        else
        {
            return false;
        }
    }
}
