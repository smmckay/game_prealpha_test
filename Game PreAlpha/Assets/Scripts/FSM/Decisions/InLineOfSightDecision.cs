﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu (menuName  = "PluggableAI/Decisions/InLineOfSight")]
public class InLineOfSightDecision : NPCDecision
{


    public override bool Decide(NPCStateController controller)
    {
        return Look(controller);
    }
    private bool Look(NPCStateController controller)
    {
        RaycastHit2D hit;
        Debug.DrawLine(controller.enemy.transform.position,controller.enemy.transform.position + (controller.enemy.transform.right * Mathf.Sign(controller.enemy.transform.localScale.x) * controller.enemyStats.lookRange));

        //Check if within sightline of enemy
        hit = Physics2D.CircleCast(controller.enemy.transform.position, controller.enemyStats.lookCircleCastRadius, controller.enemy.transform.right * Mathf.Sign(controller.enemy.transform.localScale.x), controller.enemyStats.lookRange, LayerMask.GetMask("Player"));
        if(hit && hit.collider.CompareTag("Player"))
        {
            //Set the character (player) to be targetted
            controller.targetCharacter = hit.collider.gameObject;
            return true;
        }
        else
        {
            return false;
        }
    }
}
