﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/IsDead")]
public class IsDeadDecision : NPCDecision {
    public override bool Decide(NPCStateController controller)
    {
        return controller.enemy.health <= 0;
    }
}
