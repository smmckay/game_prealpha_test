﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/InChaseRange")]
public class InChaseRangeDecision : NPCDecision
{

    public override bool Decide(NPCStateController controller)
    {
        return InRange(controller);
    }
    
    private bool InRange(NPCStateController controller)
    {
        Collider2D hit;
        hit = Physics2D.OverlapCircle(controller.enemy.transform.position, controller.enemyStats.chaseRadius, LayerMask.GetMask("Player"));
        if (hit && hit.CompareTag("Player")) {
            return true;
        }
        return false;
    }

   
}
