﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Kunai : MonoBehaviour {

    //Determined by object
    public float weight = 100f;

    //Determined by player
    [HideInInspector]public float speed;
    [HideInInspector] public float maxRange;
    [HideInInspector] public float damage;
    [HideInInspector] public Vector2 direction = new Vector2(0, 0);

    //Instance specific
    private Rigidbody2D r2d;
    private float timeActive;
    private Vector2 initialPos;
    private float dist;
    private Enemy enemy;


	// Use this for initialization
	void Start () {
		r2d = GetComponent<Rigidbody2D>();
	}

	public void Initialize(Enemy e, Vector2 dir)
    {
        speed = e.controller.enemyStats.rangedAttackSpeed;
        damage = e.controller.enemyStats.rangedAttackDmg;
        maxRange = e.controller.enemyStats.rangedAttackRange;
        direction = dir.normalized;
        initialPos = e.transform.position;
        enemy = e;

        this.transform.parent = null;
    }

    private void FixedUpdate () 
    {
        timeActive += Time.deltaTime;
        dist = Mathf.Abs(transform.position.x - initialPos.x);
         //If we hit an object get rid of the kunai
        if (dist > maxRange) 
        {
            Destroy(gameObject);
        } else 
        {
            r2d.velocity = direction * speed;
        }
       
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        int collisionLayer = other.gameObject.layer;

        //Damage player if we hit them
        if (collisionLayer == LayerMask.NameToLayer("Player"))
        {
            other.GetComponent<Character>().TakeDamage(damage, direction.x * -1 * weight);
            Destroy(gameObject);

        }
        else if (collisionLayer == LayerMask.NameToLayer("Enemy") && other != this.enemy.GetComponent<Collider2D>())
        {
            other.GetComponent<Character>().TakeDamage(damage, direction.x * -1 * weight);
            Destroy(gameObject);
        }
        //Otherwise get rid of it
        else if (collisionLayer == LayerMask.NameToLayer("Ground") || collisionLayer == LayerMask.NameToLayer("Trees"))
        {
            Destroy(gameObject);
        }
       
    }
}
