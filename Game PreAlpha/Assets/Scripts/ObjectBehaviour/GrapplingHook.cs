﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class GrapplingHook : MonoBehaviour {

    public Texture rope;
    private float speed;
    private float maxTime;
    private float pullTime;
    private float damage;
    private Vector2 direction = new Vector2(0, 0);

    private Rigidbody2D r2d;
    private Player player;
    private Enemy enemy;
    private float timeActive;

    private bool stuck = false;
    private bool enemyHit = false;
    private LineRenderer line;

    private Vector2 lastPos;
    private Vector2 currentPos;


    public enum GrappleSubState {
        Firing,
        Pulling, 
        PullingEnemy
    }
    public GrappleSubState subState;
   


    // Use this for initialization
    void Start () {
        r2d = GetComponent<Rigidbody2D>();
        line = this.gameObject.AddComponent<LineRenderer>();
        // Set the width of the Line Renderer
        line.startWidth = 0.5f;
        line.endWidth = 0.5f;
        // Set the number of vertex fo the Line Renderer
        line.positionCount = 2;
    }

    //Set variables
    public void Initialize(Player p, Vector2 dir)
    {
        speed = p.grappleSpeed;
        damage = p.grappleDamage;
        pullTime = p.grapplePullTime;
        maxTime = p.grappleTime;
        player = p;
        direction = dir.normalized;
        rope = p.controller.lvlMgr.rope;
        subState = GrappleSubState.Firing;
    }

    private void FixedUpdate () {

        timeActive += Time.deltaTime;
        switch (subState)
        {
            case GrappleSubState.Firing:
                //If we hit an object move the player towards it 
                if (stuck)
                {
                    subState = GrappleSubState.Pulling;
                    r2d.velocity = Vector2.zero;
                    timeActive = 0;
                } else if (enemyHit)
                {
                	subState = GrappleSubState.PullingEnemy;
                	r2d.velocity = Vector2.zero;
                	timeActive = 0;
                } else if (timeActive > maxTime) 
                {
                    player.controller.canGrapple = true;
                    Destroy(gameObject);
                } else 
                {
                    r2d.velocity = direction * speed;
                    CheckCollisions();
                }
                break;
            case GrappleSubState.Pulling:
                if (timeActive > pullTime) 
                {
                    player.controller.canGrapple = true;
                    Destroy(gameObject);
                } else {
                    player.controller.movement.GrapplePull(player, (transform.position - player.transform.position).normalized, transform.position);
                }
                break;
             case GrappleSubState.PullingEnemy:
             	if (timeActive > pullTime)
             	{
                    player.controller.canGrapple = true;
                    Destroy(gameObject);
             	} else
             	{
             		transform.position = enemy.transform.position;
             		enemy.controller.enemyMovement.GrapplePull(enemy, (transform.position - player.transform.position).normalized, player.transform.position, player.grappleForce / 10);
             	}
             	break;
            default:
                break;
           
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        int collisionLayer = other.gameObject.layer;

        //Damage Enemy if we hit them
        if (collisionLayer == LayerMask.NameToLayer("Enemy"))
        {

            enemy = other.GetComponent<Enemy>();
            enemy.TakeDamage(damage, 0);
            enemyHit = true;

        }
        //Propel player towards target if we can grapple to it
        else if (collisionLayer == LayerMask.NameToLayer("Grapple"))
        {
            stuck = true;
        }
        //Otherwise, just destroy object
        else if (collisionLayer == LayerMask.NameToLayer("Ladders") || collisionLayer == LayerMask.NameToLayer("Ground") || collisionLayer == LayerMask.NameToLayer("Trees"))
        {
            player.controller.canGrapple = true;
            Destroy(gameObject);
        }
       
    }

    private void CheckCollisions()
    {
        currentPos = this.transform.position;
        if (!(lastPos == new Vector2(0, 0)))
        {
            RaycastHit2D hit;

            float dist = Distance.FindDistance(transform.position, lastPos);
            Vector2 moved = currentPos - lastPos;
            Debug.DrawLine(currentPos, lastPos, Color.red, 1f);
            hit = Physics2D.CircleCast(currentPos, 0.01f, moved, dist, LayerMask.GetMask("Grapple") | LayerMask.GetMask("Enemy"));
            if (hit)
            {
                //Debug.Log("Passing Through");
                transform.position = hit.point;
                r2d.velocity = new Vector2(0f, 0f);
            }
        }
        lastPos = currentPos;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        
    }

}
