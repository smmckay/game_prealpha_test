﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BetterJumping : MonoBehaviour {

    public float m_FallMultiplier = 2.5f;
    public float m_LowJumpMultiplier = 2f;

    private Rigidbody2D m_Rigidbody2D;
    // Use this for initialization
    private void Awake()
    {
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate () {

        //Better Jumping in Unity (Board to Bits Games)
        if (m_Rigidbody2D.velocity.y < 0)
        {
            m_Rigidbody2D.gravityScale = m_FallMultiplier;

        }
        else if (m_Rigidbody2D.velocity.y > 0 && !Input.GetKey(KeyCode.Space)) 
        {
            m_Rigidbody2D.gravityScale = m_LowJumpMultiplier;
        }
        else
        {
            m_Rigidbody2D.gravityScale = 1f;
        }

    }
}
